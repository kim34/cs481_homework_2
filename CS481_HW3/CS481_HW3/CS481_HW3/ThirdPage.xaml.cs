﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ThirdPage : ContentPage
    {
        public ThirdPage(string name, int ID)
        {
            InitializeComponent();

            label_text.Text = string.Format("Hello! {0} / {1}! Welcome to Mobile Programing class", name, ID); // Display user's name and student ID.
        }

        public async void GoFirst(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync(); // if clicked the GO First button, then go to first page
        }
    }
}