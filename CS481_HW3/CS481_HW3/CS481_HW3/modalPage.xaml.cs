﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class modalPage : ContentPage
    {
        public int ID // get and set a Student ID
        {
            get;
            set;
        }
        public modalPage(string name)
        {
            InitializeComponent();

            ID = -1; // initialize ID to -1 because ID must be positive number. 

            if(string.IsNullOrWhiteSpace(name)) // if user do not enter name, Name will be "Your"
            {
                name = "Your"; 
            }

            label_Name.Text = "Insert " + name + "'s ID"; // display user's ID 
        }

        public async void Confirm(object sender, EventArgs e)
        {

            ID = Convert.ToInt32(Entry_ID.Text); // change the ID.text to intiger. 
            if (ID <= 0)    // if the inserted ID is less than 0 give alert
            {
                await DisplayAlert("ID is not valid", "Please Intert valid ID", "OK"); 
                return;
            }
            await Navigation.PopModalAsync();// if enter valid ID and click Enter button, pop the page ( go to second page)

        }

        public async void Cancel(object sender, EventArgs e)
        {
            ID = -1;
            await Navigation.PopModalAsync(); // if click cancle button, pop the page ( go to second page)

        }


    }
}