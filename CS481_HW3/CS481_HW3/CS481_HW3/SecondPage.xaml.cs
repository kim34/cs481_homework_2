﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]


    public partial class SecondPage : ContentPage
    {
        modalPage modal; // connect between second page and modal page. 
        public SecondPage()
        {
            InitializeComponent();
        }

        public async void BackPage(object sender, EventArgs e)
        {
            modal = new modalPage(Entry_Name.Text);
            await Navigation.PushModalAsync(modal); // if click the Entry_Name button, then go to the modal page ( insert student ID section). 
            
        }

        public async void NextPage(object sender, EventArgs e)
        {
            if(modal ==null || modal.ID<0)  // if Id is empty or less tan 0, give alert. 
            {
               await DisplayAlert("Please Insert Student ID"," ID is empty", "OK"); // display warning if there is no student ID. 
                return;
            }
            await Navigation.PushAsync(new ThirdPage(Entry_Name.Text,modal.ID)); // Enter the student ID then go to next page (third page).

        }


    }
}