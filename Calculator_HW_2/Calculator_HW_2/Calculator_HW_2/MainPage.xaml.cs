﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Calculator_HW_2
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {


        double result = 0;
        string operate = "";



        public MainPage()
        {
            InitializeComponent();

        }

        private void Delete_Clicked(object sender, EventArgs e)
        {
            label_1.Text = "0";
            result = 0;
         

        }

        private void Number_Clicked(object sender, EventArgs e)
        {

            if (label_1.Text == "0")
                label_1.Text = " ";
        
            

            Button button = (Button)sender;
           
            label_1.Text = label_1.Text + button.Text;
         
        }




        private void Operation_Clicked(object sender, EventArgs e)
        {

          
            result = double.Parse(label_1.Text);

            operate = ((Button)sender).Text;

            label_1.Text = "";



        }

        private void Calculate_Clicked(object sender, EventArgs e)
        {
          


            if (operate == "+")
            {

                result =  result+double.Parse(label_1.Text);

                
            }

            else

            {
                result = (result - double.Parse(label_1.Text));
               
            }

            label_1.Text = result.ToString();

        }
    }
}
